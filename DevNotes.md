# Developer Notes

These are just some notes for myself while I work on the project. They may be deleted at any point in time, without warning. The notes might reflect the current state of the project, things I want to do, or something else entirely.

## Threading Model

The player should use a simple consumer -- producer threading model. Since the main thread has control of the curses window, it will be the one displaying frames. Another thread can then be used to decode video frames and prepare them for display.

I am currently undecided as to whether there should be a single producer thread or multiple. The two potential bottlenecks in this project are video decode/transformation and writing to the console. If decoding video frames and rescaling them to the terminal screen size becomes too large of a bottleneck, then increasing the number of threads could help. Writing to the terminal is more difficult to control/optimize, and I do not want to switch to another library like SDL; a primary goal of the project is to render to existing terminal emulators.

## Decoding Video

The high-level goal is to go from a video file on disk to an array of characters that can be displayed in curses. In order to do this, my idea is to decode single frames of video from file into an RGB buffer, then rescale it to an appropriate size for display. We can then do a pixel-to-character mapping to prepare the frame for display.

I am decoding video frames using the ffmpeg `libav*` libraries such as `libavformat`, `libavutil`, `libswscale`, etc. Library calls happen on a child thread, but `libav` may use additional threads/hardware acceleration to do the actual decode operation.

## Curses Rendering

Once a frame has been transformed into the appropriate size, it must then be mapped to an array of characters for display onto the screen. I would like to support multiple drawing modes using combinations of ascii/unicode and monochrome/colour. For most modes, the most flexible curses rendering will be to loop through the screen and draw one character at a time. However, this method is also likely to be the slowest; it may be possible to improve performance by drawing chunks or even entire lines of output at a time.

## Platform Support

I would love to support Windows and Linux, since those are the platforms that I have available to test locally. The ffmpeg libraries should be available on both platforms, but curses support is provided by separate libraries with different maintainers. `ncurses` will be used on Linux and `PDCurses` on Windows.

## Other Ideas

These are ideas that I would like to implement, but don't see a clear way on how to get them to work.

### Playing Audio

It would be nice if the player could also play audio, but I don't want to include another large dependency. I have seen people use libraries like SFML or SDL, but these libraries also have image drawing functionality. It wouldn't make much sense to include those in a program whose purpose is to avoid drawing pixels!
