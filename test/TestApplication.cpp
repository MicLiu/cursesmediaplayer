#include <curses.h>
#include <fstream>
#include <iostream>
#include <string>
#include "mediaplayer.hpp"

#include <chrono>
#include <thread>

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "Usage: ./TestApplication filename" << std::endl;
        return 1;
    }

    initscr();
    curs_set(0);

    MediaPlayer player(false);
    if (!player.loadMedia(argv[1])) {
        std::cerr << "Failed to load media" << std::endl;
        endwin();
        exit(1);
    }
    player.setWindow(stdscr);
    for (int i = 0; i < 1000; i++) {
        if (!player.getFrame()) {
            std::cerr << "Failed to get frame " << i << std::endl;
            endwin();
            exit(1);
        }
        player.displayFrame();
    }
    wclear(stdscr);
    wrefresh(stdscr);
    endwin();
    std::cout << "===== pCodecContext =====" << std::endl;
    std::cout << "Video size: (" 
              << player.pCodecContext->width
              << " x "
              << player.pCodecContext->height
              << ")"
              << std::endl;
    std::cout << "Pixel format: "
              << player.pCodecContext->pix_fmt
              << std::endl;

    std::cout << "===== pFrame =====" << std::endl;
    std::cout << "Video size: (" 
              << player.pFrame->width
              << " x "
              << player.pFrame->height
              << ")"
              << std::endl;
    std::cout << "Pixel format: "
              << player.pFrame->format
              << std::endl;
    std::cout << "Linesize: ["
              << (int)player.pFrame->linesize[0]
              << ", "
              << (int)player.pFrame->linesize[1]
              << ", "
              << (int)player.pFrame->linesize[2]
              << ", "
              << (int)player.pFrame->linesize[3]
              << ", "
              << (int)player.pFrame->linesize[4]
              << ", "
              << (int)player.pFrame->linesize[5]
              << ", "
              << (int)player.pFrame->linesize[6]
              << ", "
              << (int)player.pFrame->linesize[7]
              << "]"
              << std::endl;

    std::cout << "===== pFrameRGB =====" << std::endl;
    std::cout << "Video size: (" 
              << player.pFrameRGB->width
              << " x "
              << player.pFrameRGB->height
              << ")"
              << std::endl;
    std::cout << "Pixel format: "
              << player.pFrameRGB->format
              << std::endl;
    std::cout << "Linesize: "
              << (int)player.pFrameRGB->linesize[0]
              << ", "
              << (int)player.pFrameRGB->linesize[1]
              << ", "
              << (int)player.pFrameRGB->linesize[2]
              << ", "
              << (int)player.pFrameRGB->linesize[3]
              << ", "
              << (int)player.pFrameRGB->linesize[4]
              << ", "
              << (int)player.pFrameRGB->linesize[5]
              << ", "
              << (int)player.pFrameRGB->linesize[6]
              << ", "
              << (int)player.pFrameRGB->linesize[7]
              << "]"
              << std::endl;

    // std::ofstream outFile;
    // outFile.open("output.ppm");
    // outFile << "P6" << "\n"
    //         << player.pFrameRGB->width << " " << player.pFrameRGB->height << "\n"
    //         << "255" << "\n";
    // for (int i = 0; i < player.pFrameRGB->height; i++) {
    //     outFile.write(((const char*)player.pFrameRGB->data[0]) + i*player.pFrameRGB->linesize[0], player.pFrameRGB->width * 3);
    // }
    // outFile.close();
    return 0;
}
