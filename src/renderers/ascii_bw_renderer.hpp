#ifndef ASCII_BW_RENDERER_HPP
#define ASCII_BW_RENDERER_HPP

#include "curses_renderer.hpp"

class AsciiBWRenderer : public CursesRenderer {
public:
    virtual void displayFrame(WINDOW* window, AVFrame* pFrameRGB);
};

#endif