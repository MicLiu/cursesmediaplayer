#include "ascii_bw_renderer.hpp"

#include <curses.h>

extern "C" {
#include <libavutil/frame.h>
}

void AsciiBWRenderer::displayFrame(WINDOW* window, AVFrame* pFrameRGB) {
    static const char pixelMap[] = " .:-=+*#%@";
    int winWidth;
    int winHeight;
    int pos;
    uint8_t r;
    uint8_t g;
    uint8_t b;

    getmaxyx(window, winHeight, winWidth);
    for (int y = 0; y < winHeight; y++) {
        for (int x = 0; x < winWidth; x++) {
            pos = y * pFrameRGB->linesize[0] + x * 3;
            r = pFrameRGB->data[0][pos];
            g = pFrameRGB->data[0][pos + 1];
            b = pFrameRGB->data[0][pos + 2];
            // mvwaddch(window, y, x, pixelMap[(int) (sqrt(pow(r, 2) + pow(g, 2) + pow(b, 2))/442*10)]);
            mvwaddch(window, y, x, pixelMap[(int)((r + g + b) * 10 / 765)]);
        }
    }
    wrefresh(window);
}