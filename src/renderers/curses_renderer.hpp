#ifndef IDRAWER_HPP
#define IDRAWER_HPP

#include <curses.h>

extern "C" {
#include <libavutil/frame.h>
}

class CursesRenderer {
public:
    virtual ~CursesRenderer(){};
    virtual void displayFrame(WINDOW* window, AVFrame* pFrameRGB) = 0;
};

#endif