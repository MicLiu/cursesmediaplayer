#ifndef MEDIAPLAYER_HPP
#define MEDIAPLAYER_HPP

#include <curses.h>

#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <string>
#include <thread>

extern "C" {
#include <libavformat/avformat.h>
}

#include "renderers/curses_renderer.hpp"

class MediaPlayer {
public:
    MediaPlayer(const bool useColours);
    ~MediaPlayer();

    bool loadMedia(std::string filename);
    void setWindow(WINDOW* win);

    // Threaded playback
    bool play();
    void pause();
    void stop();
    bool seek(int time);

    bool isPlaying();

    // Synchronous playback
    bool nextFrame();
    bool playFrame();

    // private:
    time_t lastFrameTime;
    std::thread* pPlayerThread;
    bool flush;
    int stream_index;
    volatile bool playing;
    bool useColours;
    void (MediaPlayer::*fn_displayFrame)();

    AVFormatContext* pFormatContext;
    AVCodecParameters* pCodecParameters;
    AVCodec* pCodec;
    AVCodecContext* pCodecContext;
    AVPacket* pPacket;
    AVFrame* pFrame;
    AVFrame* pFrameRGB;
    struct SwsContext* pSwsContext;

    WINDOW* pWindow;
    int winWidth;
    int winHeight;

    void do_play();
    void displayFrame();
    void do_displayFrameBW();
    bool getFrame();
    bool initSwsContext();
    void cleanup();
};

#endif
