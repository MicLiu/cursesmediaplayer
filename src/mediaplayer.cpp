#include <cassert>
#include <ctime>
#include <iostream>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#include "mediaplayer.hpp"

/* = = = = = = = = = = = = *\
|* Public Member Functions *|
\* = = = = = = = = = = = = */

/**
 * @brief Construct a new MediaPlayer object
 *
 */
MediaPlayer::MediaPlayer(const bool useColours)
    : lastFrameTime(0),
      pPlayerThread(nullptr),
      flush(false),
      stream_index(0),
      playing(false),
      useColours(useColours),
      fn_displayFrame(&MediaPlayer::do_displayFrameBW),
      pFormatContext(nullptr),
      pCodecParameters(nullptr),
      pCodec(nullptr),
      pCodecContext(nullptr),
      pPacket(nullptr),
      pFrame(nullptr),
      pFrameRGB(nullptr),
      pSwsContext(nullptr),
      pWindow(nullptr),
      winWidth(0),
      winHeight(0) {}

/**
 * @brief Destroy the MediaPlayer object
 *
 */
MediaPlayer::~MediaPlayer() {
    cleanup();
    // TODO: Audit MediaPlayer for anything that needs to be destroyed
}

/**
 * @brief Loads media file into MediaPlayer
 *
 * @param filename
 * @return true
 * @return false
 */
bool MediaPlayer::loadMedia(std::string filename) {
    // Stop playback & de-allocate any previously allocated structures
    cleanup();

    if ((pFormatContext = avformat_alloc_context()) == nullptr) {
        std::cerr << "Error: Could not allocate format context" << std::endl;
        return false;
    }
    // Read the media headers into the FormatContext
    if (avformat_open_input(&pFormatContext, filename.c_str(), nullptr, nullptr) != 0) {
        std::cerr << "Error: Could not open file" << filename << std::endl;
        return false;
    }
    // Get audio/video stream headers from file
    if (avformat_find_stream_info(pFormatContext, nullptr) < 0) {
        std::cerr << "Error: Could not get streams from file" << std::endl;
        return false;
    }
    stream_index = av_find_best_stream(pFormatContext, AVMEDIA_TYPE_VIDEO, -1, -1, &pCodec, 0);
    if (stream_index < 0) {
        std::cerr << "Error: Could not find video stream in file" << std::endl;
        return false;
    }
    pCodecParameters = pFormatContext->streams[stream_index]->codecpar;
    if ((pCodecContext = avcodec_alloc_context3(pCodec)) == nullptr) {
        std::cerr << "Error: Failed to allocate codec context" << std::endl;
        return false;
    }
    // Get Codec Context
    if (avcodec_parameters_to_context(pCodecContext, pCodecParameters) < 0) {
        std::cerr << "Error: Failed to convert codec parameters to context" << std::endl;
        return false;
    }
    // Open the codec
    if (avcodec_open2(pCodecContext, pCodec, nullptr) < 0) {
        std::cerr << "Error: Failed to initialize codec" << std::endl;
        return false;
    }
    // Allocate packet and frame
    if ((pPacket = av_packet_alloc()) == nullptr) {
        std::cerr << "Error: Failed to allocate packet" << std::endl;
        return false;
    }
    if ((pFrame = av_frame_alloc()) == nullptr) {
        std::cerr << "Error: Failed to allocate frame" << std::endl;
        return false;
    }
    initSwsContext();
    return true;
}

/**
 * @brief Sets the window that the video will be played back on and loads basic
 *        window properties.
 *
 * For threaded playback, the player should have exclusive access to the window
 * contents while playing. The user should pause the video first if the window
 * is to be used for other purposes.
 *
 * For synchronous playback, the window content can be modified in between
 * frames at will, but changes to properties of the window (eg. size) must be
 * succeeded by a call to setWindow before any playing functions are called
 *
 * @param win CURSES window to display on
 *
 * @return true if successfully set window properties
 */
void MediaPlayer::setWindow(WINDOW* win) {
    pWindow = win;
    if (win == nullptr) {
        winWidth = 0;
        winHeight = 0;
        sws_freeContext(pSwsContext);
        pSwsContext = nullptr;
    } else {
        getmaxyx(pWindow, winHeight, winWidth);
        initSwsContext();
    }
}

/**
 * @brief Spawns a new player thread to begin pushing frames to pWindow
 *
 * @return true   Successfully created new thread
 * @return false  Failed to create new thread
 */
bool MediaPlayer::play() {
    if (pWindow == nullptr) {
        std::cerr << "MediaPlayer must be attached to a valid curses window before playing" << std::endl;
        return false;
    }
    if (playing) {
        return true;
    }
    if (!pPlayerThread) {
        pPlayerThread = new std::thread(&MediaPlayer::do_play, this);
    }
    if (!pPlayerThread) {
        std::cerr << "Failed to create player thread" << std::endl;
        return false;
    }
    playing = true;
    return true;
}

/**
 * @brief Pause the playback by destroying player thread
 */
void MediaPlayer::pause() {
    playing = false;
    pPlayerThread->join();
    delete pPlayerThread;
    pPlayerThread = nullptr;
    lastFrameTime = -1;
}

/**
 * @brief Stop playback and reset media to frame 0
 */
void MediaPlayer::stop() {
    pause();
    seek(0);
}

/**
 * @brief Seek the video playback to time seconds
 *
 * @param  time   number of seconds to seek to
 * @return true   when seek is successful
 * @return false  when seek fails
 */
bool MediaPlayer::seek(int time) { return false; }

bool MediaPlayer::isPlaying() { return playing; }

/**
 * @brief Displays the next frame in the video to the screen.
 *
 * @post pPacket and pFrame will be unreferenced
 *
 * @return true   new frame successfully written to window
 * @return false  failed to load and display next frame
 */
bool MediaPlayer::nextFrame() {
    if (getFrame()) {
        displayFrame();
        return true;
    }
    return false;
}

/**
 * @brief Display next frame in video taking into account time elapsed since
 * last call of this function
 *
 * @return true   new frame successfully written to window
 * @return false  failed to load and display next frame
 */
bool MediaPlayer::playFrame() {
    if (lastFrameTime != -1) {
        // Compute next frame to play
        time_t dt = std::time(nullptr) - lastFrameTime;
        // seek(0);
    }
    lastFrameTime = std::time(nullptr);
    return true;
}

/* = = = = = = = = = = = = = *\
 * Private Member Functions  *|
 * = = = = = = = = = = = = = */

/**
 * @brief Threaded function which continuously updates the window with new
 * frame information.
 *
 * This function polls to see if the player is playing video. When the value
 * is false, do_play will block until new media is to be played.
 */
void MediaPlayer::do_play() {
    while (playing) {
        if (!playFrame()) {
            break;
        }
        // sleep
    }
}

/**
 * @brief Fills pFrame and pFrameRGB with a single frame's worth of data,
 *        reading additional packets as necessary.
 *
 * @pre loadMedia must have been called with valid media file
 * @pre pPacket, pFrame, pFrameRGB must have been unref'd
 */
bool MediaPlayer::getFrame() {
    // Try to get a frame from the decoder
    while (avcodec_receive_frame(pCodecContext, pFrame)) {
        if (flush) {
            // Already in flush mode -- no more frames
            std::cerr << "Flush mode end" << std::endl;
            return false;
        }
        // Read packets from the file until a video packet arrives
        do {
            if (av_read_frame(pFormatContext, pPacket)) {
                // No more packets to read from the video stream, so initiate drain mode
                std::cerr << "Flush mode start" << std::endl;
                flush = true;
                if (avcodec_send_packet(pCodecContext, nullptr)) {
                    std::cerr << "Failed to put decoder into drain mode" << std::endl;
                    return false;
                }
            }
        } while (pPacket->stream_index != stream_index);

        // Send the packet to the decoder
        if (avcodec_send_packet(pCodecContext, pPacket)) {
            std::cerr << "Failed to send packet to decoder" << std::endl;
            return false;
        }
    }
    // Use libswscale to convert native pixel format to 24 bit RGB and scale frame size as appropriate
    sws_scale(pSwsContext, pFrame->data, pFrame->linesize, 0, pFrame->height, pFrameRGB->data, pFrameRGB->linesize);
    return true;
}

/**
 * @brief Draws frame stored in pFrame to pWindow, scaling when necessary.
 *
 * @pre loadMedia must have been called with valid media file
 * @pre pFrameRGB must have been populated with a valid frame to push
 *      (ie. by getFrame)
 */
inline void MediaPlayer::displayFrame() { (this->*fn_displayFrame)(); }

void MediaPlayer::do_displayFrameBW() {
    static const char pixelMap[] = " .:-=+*#%@";
    int pos;
    uint8_t r;
    uint8_t g;
    uint8_t b;
    for (int y = 0; y < winHeight; y++) {
        for (int x = 0; x < winWidth; x++) {
            pos = y * pFrameRGB->linesize[0] + x * 3;
            r = pFrameRGB->data[0][pos];
            g = pFrameRGB->data[0][pos + 1];
            b = pFrameRGB->data[0][pos + 2];
            // mvwaddch(pWindow, y, x, pixelMap[(int) (sqrt(pow(r, 2) + pow(g, 2) + pow(b, 2))/442*10)]);
            mvwaddch(pWindow, y, x, pixelMap[(int)((r + g + b) * 10 / 765)]);
        }
    }
    wrefresh(pWindow);
}

/**
 * @brief Initialize the scaling context if both curses window and media file
 *        loaded via calls to setWindow and loadMedia, respectively.
 *
 * @return true  Software scaling context successfully created
 * @return false otherwise
 */
bool MediaPlayer::initSwsContext() {
    // Cannot initialize scaling context without dimensions of both screen and video
    if (!pCodecParameters || !pWindow) {
        return false;
    }
    // Free any previously allocated sws context
    if (pSwsContext) {
        // Do not set to nullptr because we are about to create a new context
        sws_freeContext(pSwsContext);
    }
    // Create scaling context
    pSwsContext = sws_getContext(pCodecContext->width, pCodecContext->height, pCodecContext->pix_fmt, winWidth,
                                 winHeight, AV_PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);
    if (!pSwsContext) {
        std::cerr << "Error: Failed to get SWScale context" << std::endl;
        return false;
    }
    // Allocate RGB frame buffer if not allocated before
    if (!pFrameRGB) {
        if ((pFrameRGB = av_frame_alloc()) == nullptr) {
            std::cerr << "Error: Failed to allocate RGB frame" << std::endl;
            return false;
        }
        pFrameRGB->width = winWidth;
        pFrameRGB->height = winHeight;
        pFrameRGB->format = AV_PIX_FMT_RGB24;
    }
    // If curses window dimensions has changed since frame buffer was created, free the old buffers (and set to nullptr)
    if (pFrameRGB->width != winWidth || pFrameRGB->height != winHeight) {
        for (int i = 0; i < 8; i++) {
            if (pFrameRGB->data[i]) {
                av_freep(&pFrameRGB->data[i]);
            }
        }
    }
    // If buffers are nullptr, allocate image data
    if (!pFrameRGB->data[0]) {
        if (av_image_alloc(pFrameRGB->data, pFrameRGB->linesize, winWidth, winHeight, AV_PIX_FMT_RGB24, 16) < 0) {
            std::cerr << "Error: Failed to allocate pFrameRGB image data" << std::endl;
            return false;
        }
    }
    return true;
}

/**
 * @brief Stops the player thread and frees all dynamically allocated memory
 */
void MediaPlayer::cleanup() {
    playing = false;
    if (pPlayerThread) {
        pPlayerThread->join();
        delete pPlayerThread;
    }
    if (pFrame) {
        av_frame_free(&pFrame);
    }
    if (pFrameRGB) {
        for (int i = 0; i < 8; i++) {
            if (pFrameRGB->data[i]) {
                av_freep(&pFrameRGB->data[i]);
            }
        }
        av_frame_free(&pFrameRGB);
    }
    if (pPacket) {
        av_packet_free(&pPacket);
    }
    if (pCodecContext) {
        avcodec_free_context(&pCodecContext);
    }
    if (pFormatContext) {
        avformat_close_input(&pFormatContext);
    }
    if (pSwsContext) {
        sws_freeContext(pSwsContext);
    }
}