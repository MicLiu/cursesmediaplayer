#include <iostream>
#include <curses.h>

#include "mediaplayer.hpp"

MediaPlayer player(false);

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "Usage: ./VideoPlayer filename" << std::endl;
    }
    
    if (player.loadMedia(argv[1])) {
        std::cout << "Successfully loaded " << argv[1] << std::endl;
    }
    if (player.loadMedia(argv[1])) {
        std::cout << "Successfully loaded " << argv[1] << std::endl;
    }
    return 0;
}